//
//  ViewController.swift
//  Demo1
//
//  Created by Javier Yllescas on 11/11/19.
//  Copyright © 2019 Javier Yllescas. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var lb01: UILabel!
    @IBOutlet weak var btn01: UIButton!
    @IBOutlet weak var tf01: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lb01.text = "Hola Javier"
        btn01.layer.cornerRadius = 50
        
    }

    var suma_par = 0
    @IBAction func ejecutar(_ sender: Any) {
        suma_par = suma_par + 2
        print("Pulsos: \(suma_par)")
        tf01.text = String(suma_par)
    }

}

